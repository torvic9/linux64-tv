# Based on the file created for Arch Linux by:
# Tobias Powalowski <tpowa@archlinux.org>
# Thomas Baechler <thomas@archlinux.org>

# Maintainer: Tor Vic <torvic9 AT mailbox DOT org>
# NO WEAPONS FOR UKRAINE

pkgbase=linux64-tv
pkgname=('linux64-tv' 'linux64-tv-headers')
_basekernel=6.4
_sub=13
_kernelname=-tv
pkgver=${_basekernel}.${_sub/-/}
_tagver=${pkgver}${_kernelname}
pkgrel=1
arch=('x86_64')
url="http://www.kernel.org/"
license=('GPL2')
makedepends=('xmlto' 'kmod' 'inetutils' 'bc' 'cpio' 'perl'
            'python' 'elfutils' 'git' 'libelf' 'pahole' 'gettext')
options=('!strip' '!ccache')
source=(git+https://gitlab.com/torvic9/linux-stable.git#tag=v${_tagver}
    #git+https://gitlab.com/torvic9/linux-stable.git#branch=torvic-6.4
    #
    # the main kernel config files
    'config.x86_64' 'config.x270' 'config.zen2' 'x509.genkey' "$pkgbase.preset"
    # patches
    'cpupower.patch'
    # 'sched-eevdf-20230818.patch'
    'sched-tv-tune-some-cfs-defaults.patch' 'sched-pelt-faster.patch'
    # 'prjc-6.4-r1-tv.patch' 'prjc-disable-mmcid.diff'
    #
)

validpgpkeys=(
  'ABAF11C65A2970B130ABE3C479BE3E4300411886'  # Linus Torvalds
  '647F28654894E3BD457199BE38DBBDC86092693E'  # Greg Kroah-Hartman
)

sha256sums=('SKIP'
            'c531af0f63a09f85472bf9ea1c9153f5dcd9a38778bd318048c3465329ebe783'
            'bd897b53568353023c366254a4eba53aa648431872cdb876a10a7112c2694bca'
            'f8b9895c5656167600bfea6c96c48ab49824190d2870c05539795ce476c9ebf1'
            '1ec8c2d92fcd92b59b4f87559198fde0efde0b8e64c82b144cd58d79c72fba14'
            'ecf89e91ae7ea72f251a6f65a4c14710c26793a7e8c463be11807fa113d984ff'
            '962f2f269af9bd75ac3a3579411f1caa4bcfd082ce4abb29ebdb09a99b37d155'
            'ba1639d74ce0203a2105974e7cca41ea4a28c8e6124f0033b576debcee7897b3'
            '52afbb7578d4b9111560dd4e9fe965fe55b77516097981774353a6858b5394f2')

export KBUILD_BUILD_USER=$pkgbase
export KBUILD_BUILD_HOST=arch
export KBUILD_BUILD_TIMESTAMP="$(date -Ru${SOURCE_DATE_EPOCH:+d @$SOURCE_DATE_EPOCH})"

# signing
_signing=1
# edit the paths below to point to your signing keys
_key="$HOME/build/keys/vd-kernel.key"
_pub="$HOME/build/keys/vd-kernel.pub"

# custom clang path
# export PATH=/opt/clang16/bin:$PATH
_clang=0

# export PATH=/opt/gcc13/bin:$PATH

if [[ ${_clang} -eq 1 ]]; then
	LLVMOPTS="LLVM=1 LLVM_IAS=1"
	CLANGOPTS="CC=clang LD=ld.lld"
else
	LLVMOPTS=""
	CLANGOPTS=""
fi

TB=$(tput bold)
TN=$(tput sgr0)

prepare() {

  cd "${srcdir}/linux-stable"

  sed -i '36s/false/true/' ./scripts/setlocalversion
  #sed -i -e "/^EXTRAVERSION/ s/$/-tv/" Makefile
  #echo "${_kernelname}" > localversion.10-pkgname
  echo "-${pkgrel}" > localversion.20-pkgrel

  echo -e "\n${TB}* APPLYING PATCHES${TN}"
  local src
  for src in "${source[@]}"; do
  	src="${src%%::*}"
  	src="${src##*/}"
  	[[ $src = *.patch || $src = *.diff ]] || continue
  	echo "- Applying $src..."
  	patch -Np1 < "../$src"
  done

  # kernel config
  echo -e "\n${TB}* KERNEL CONFIGURATION${TN}"
  local _config
  echo "---- Select configuration file:"
  echo "${TB}1)${TN} Default"
  echo "${TB}2)${TN} Zen2"
  echo "${TB}3)${TN} X270"
  while true ; do
  	read -p "Enter number (1-3): " _config
	  case ${_config} in
		1) cat ../config.x86_64 > ./.config && break ;;
		2) cat ../config.zen2 > ./.config && break ;;
		3) cat ../config.x270 > ./.config && break ;;
		*) echo "Please enter a number (1-3)!" && continue ;;
  	  esac
  done

  if [[ ${_signing} -eq 1 ]] ; then
  	cat ${_key} > ./certs/vd-kernel.key
  	cat ${_pub} > ./certs/vd-kernel.pub
  	sed -i "s|signing_key.pem|vd-kernel.key|" ./.config
  	sed -ri "s|^(CONFIG_SYSTEM_TRUSTED_KEYS=).*|\1\"certs/vd-kernel.pub\"|" ./.config
  else
  	cat ../x509.genkey > ./certs/x509.genkey
  fi

  sed -i '2iexit 0' ./scripts/depmod.sh

  make $LLVMOPTS oldconfig

  # get kernel version
  make $LLVMOPTS -s kernelrelease > version
  printf "\n  Prepared %s version %s\n" "$pkgbase" "$(<version)"
  read -p "---- Enter 'y' for nconfig: " NCONFIG
  [[ $NCONFIG == "y" ]] && make $LLVMOPTS nconfig

  # rewrite configuration
  yes '' | make $LLVMOPTS config >/dev/null
}

build() {
  cd "${srcdir}/linux-stable"

  # build!
  make $LLVMOPTS LOCALVERSION= bzImage modules

  # build cpupower
  make $LLVMOPTS -C tools/power/cpupower

  # build kcpuid
  make $LLVMOPTS -C tools/arch/x86/kcpuid
}

package_linux64-tv() {
  pkgdesc="The ${pkgbase/linux/Linux} kernel and modules"
  depends=('coreutils' 'linux-firmware' 'kmod' 'mkinitcpio>=27')
  optdepends=('crda: to set the correct wireless channels of your country')
  provides=(VIRTUALBOX-GUEST-MODULES)

  cd "${srcdir}/linux-stable"

  local kernver="$(<version)"
  local modulesdir="$pkgdir/usr/lib/modules/$kernver"

  # systemd expects to find the kernel here to allow hibernation
  # https://github.com/systemd/systemd/commit/edda44605f06a41fb86b7ab8128dcf99161d2344
  install -Dm644 "$(make $LLVMOPTS -s image_name)" "$modulesdir/vmlinuz"

  # Used by mkinitcpio to name the kernel
  echo "${pkgbase}" | install -Dm644 /dev/stdin "$modulesdir/pkgbase"

  # make room for external modules
  local _extramodules="extramodules-${pkgbase}"
  ln -s "../${_extramodules}" "$modulesdir/extramodules"
  # add real version for building modules and running depmod from hook
  echo "${kernver}" |
    install -Dm644 /dev/stdin "${pkgdir}/usr/lib/modules/${_extramodules}/version"

  echo -e "\n${TB}* INSTALLING MODULES${TN}"
  make $LLVMOPTS LOCALVERSION= INSTALL_MOD_PATH="${pkgdir}/usr" INSTALL_MOD_STRIP=1 modules_install

  # remove build and source links
  rm "${modulesdir}/source"
  rm "${modulesdir}/build"
  [[ -f ./certs/vd-kernel.key ]] && rm ./certs/vd-kernel.key

  # add mkinitcpio preset (not strictly needed)
  install -Dm644 "$srcdir/${pkgbase}.preset" "$pkgdir/etc/mkinitcpio.d/${pkgbase}.preset"

  # depmod
  depmod -b "${pkgdir}/usr" -F System.map "${kernver}"
}

package_linux64-tv-headers() {
  pkgdesc="Header files and scripts for building modules for ${pkgbase/linux/Linux} kernel"

  cd "${srcdir}/linux-stable"
  local kernver="$(<version)"
  local _builddir="${pkgdir}/usr/lib/modules/${kernver}/build"

  echo -e "\n${TB}* INSTALLING HEADERS${TN}"
  install -Dt "${_builddir}" -m644 Makefile .config Module.symvers System.map version vmlinux localversion.*
  install -Dt "${_builddir}/kernel" -m644 kernel/Makefile
  install -Dt "${_builddir}/arch/x86" -m644 "arch/x86/Makefile"
  #mkdir "${_builddir}/.tmp_versions"

  cp -t "${_builddir}" -R --preserve=mode,timestamps --no-dereference include scripts

  # add objtool for external module building and enabled VALIDATION_STACK option
  install -Dt "${_builddir}/tools/objtool" tools/objtool/objtool

  # add cpupower
  install -Dt "${_builddir}/tools/cpupower" tools/power/cpupower/cpupower
  install -Dt "${_builddir}/tools/cpupower" tools/power/cpupower/libcpupower.so.0.0.1

  # add kcpuid
  install -Dt "${_builddir}/tools/kcpuid" -m755 tools/arch/x86/kcpuid/kcpuid
  install -Dt "${_builddir}/tools/kcpuid" -m444 tools/arch/x86/kcpuid/cpuid.csv

  # add xfs and shmem for aufs building
  mkdir -p "${_builddir}"/{fs/xfs,mm}

  cp -t "${_builddir}/arch/x86" -R --preserve=mode,timestamps --no-dereference "arch/x86/include"
  install -Dt "${_builddir}/arch/x86/kernel" -m644 "arch/x86/kernel/asm-offsets.s"

  install -Dt "${_builddir}/drivers/md" -m644 drivers/md/*.h
  install -Dt "${_builddir}/net/mac80211" -m644 net/mac80211/*.h

  # http://bugs.archlinux.org/task/13146
  install -Dt "${_builddir}/drivers/media/i2c" -m644 drivers/media/i2c/msp3400-driver.h

  # http://bugs.archlinux.org/task/20402
  install -Dt "${_builddir}/drivers/media/usb/dvb-usb" -m644 drivers/media/usb/dvb-usb/*.h
  install -Dt "${_builddir}/drivers/media/dvb-frontends" -m644 drivers/media/dvb-frontends/*.h
  install -Dt "${_builddir}/drivers/media/tuners" -m644 drivers/media/tuners/*.h

  # copy in Kconfig files
  find . -name Kconfig\* -exec install -Dm644 {} "${_builddir}/{}" \;

  # remove unneeded stuff
  echo -e "\n${TB}* REMOVING UNNEEDED FILES${TN}"
  # remove unneeded architectures
  local _arch
  for _arch in "${_builddir}"/arch/*/; do
  	[[ ${_arch} == */x86/ ]] && continue
  	rm -r "${_arch}"
  done

  # remove files already in linux-docs package
  rm -r "${_builddir}/Documentation"

  # remove broken symlinks
  find -L "${_builddir}" -type l -printf 'Removing %P\n' -delete

  # remove loose objects"
  find "${_builddir}" -type f -name '*.o' -printf 'Removing %P\n' -delete

  # strip scripts directory
  echo -e "\n${TB}* STRIPPING${TN}"
  local file
  while read -rd '' file; do
  	case "$(file -Sib "$file")" in
    		application/x-sharedlib\;*)      # Libraries (.so)
      		  strip -v $STRIP_SHARED "$file" ;;
    		application/x-archive\;*)        # Libraries (.a)
      		  strip -v $STRIP_STATIC "$file" ;;
    		application/x-executable\;*)     # Binaries
      		  strip -v $STRIP_BINARIES "$file" ;;
    		application/x-pie-executable\;*) # Relocatable binaries
      		  strip -v $STRIP_SHARED "$file" ;;
  	esac
  done < <(find "${_builddir}" -type f -perm -u+x ! -name vmlinux -print0)

  strip -v $STRIP_STATIC "${_builddir}/vmlinux"

  echo -e "\n${TB}* SYMLINKING${TN}"
  mkdir -p "$pkgdir/usr/src"
  ln -sr "$_builddir" "$pkgdir/usr/src/$pkgbase"
}
