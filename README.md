## tv kernel 6.4 for Arch-compatible desktop systems

- better support for clang
- support for module signing
- block device LED trigger
- includes recent cpupower
- patches from Clear Linux
- patches from linux-hardened
- patches from Zen
- various fixes, optimisations and backports
- EEVDF scheduler
- sched/tip backports

See PKGBUILD for source and more details.
